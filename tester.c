#if defined(SCUTEST_IMPLEMENTATION) || ! defined(SCUTEST_H)
#define _XOPEN_SOURCE 700
#include <dirent.h>
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <sys/ioctl.h>

#if ! defined(SCUTEST_NO_STDLIB) && ! defined(SCUTEST_EXIT_FUNC)
#define SCUTEST_EXIT_FUNC(R) exit(R)
#elif defined(SCUTEST_NO_STDLIB)
#define SCUTEST_EXIT_FUNC(R)
#endif

#if ! defined(SCUTEST_NO_STDLIB) && !(defined SCUTEST_GETENV_FUNC)
#define SCUTEST_GETENV_FUNC(R) getenv(R)
#elif defined(SCUTEST_NO_STDLIB)
#define SCUTEST_GETENV_FUNC(R) NULL
#endif

#define SCUTEST_GETENV_FUNC_OR_DEFAULT(R, D) (SCUTEST_GETENV_FUNC(R) ? SCUTEST_GETENV_FUNC(R) : D)

#ifndef SCUTEST_NO_STDIO
#include <stdarg.h>
#include <stdio.h>
#define SCUTEST_PRINTF(...) dprintf(2, __VA_ARGS__)
#define SCUTEST_VPRINTF(fmt, ...) do { va_list args; va_start(args, fmt); vdprintf(2, fmt, args); va_end(args); } while(0)
#define SCUTEST_PERROR(MSG) perror(MSG)
#define SCUTEST_FLUSH() fflush(NULL)
#else
#define SCUTEST_PRINTF(...)
#define SCUTEST_VPRINTF(...)
#define SCUTEST_PERROR(MSG)
#define SCUTEST_FLUSH()
#endif

#if ! defined(SCUTEST_NO_BUFFER) && ! defined(SCUTEST_NO_STDLIB)
#include <fcntl.h>
#endif

#ifndef SCUTEST_H
#include "tester.h"
#endif

#ifndef SCUTEST_NO_STDLIB
#include <stdlib.h>
#define SCUTEST_ATOI(nptr) atoi(nptr)

static char SCUTEST_TEMP_WORKING_DIR[SCUTEST_TEMP_FILE_NAME_MAX_LEN - 8];
const char* getTempDirForTest() {
    if (!SCUTEST_TEMP_WORKING_DIR[0]) {
        SCUTEST_PRINTF("A temporary directory wasn't created for this test");
        exit(-1);
    }
    return SCUTEST_TEMP_WORKING_DIR;
}

static void SCUTEST_createTempWorkingDir() {
    strcpy(SCUTEST_TEMP_WORKING_DIR, "/tmp/.scuXXXXXX");
    mkdtemp(SCUTEST_TEMP_WORKING_DIR);
}

int createTempFileForTest(char* tempFile) {
    strcpy(tempFile, getTempDirForTest());
    strcat(tempFile, "/XXXXXX");
    return mkstemp(tempFile);
}

const char* createTempDirForTest(char* tempDir) {
    strcpy(tempDir, getTempDirForTest());
    strcat(tempDir, "/DXXXXXX");
    char* name = mkdtemp(tempDir);
    return name;
}

static void destroyTempWorkingDir() {
    if(SCUTEST_TEMP_WORKING_DIR) {
        if(!fork()) {
            execlp("rm", "rm", "-fr", SCUTEST_TEMP_WORKING_DIR, NULL);
            _exit(-1);
        }
        wait(NULL);
    }
    SCUTEST_TEMP_WORKING_DIR[0] = 0;
}

#else
#define SCUTEST_ATOI(nptr) (nptr[0] >= '0' && nptr[0] <= '9' ? nptr[0] - '0' : -1)
#endif

#ifndef SCUTEST_MAX_NUM_TESTS
#define SCUTEST_MAX_NUM_TESTS 1024
#endif

#ifndef SCUTEST_MAX_NUM_FIXTURES
#define SCUTEST_MAX_NUM_FIXTURES 100
#endif

#ifndef SCUTEST_BUFFER_READ_SIZE
#define SCUTEST_BUFFER_READ_SIZE 255
#endif

#ifndef SCUTEST_DEFAULT_TIMEOUT
#define SCUTEST_DEFAULT_TIMEOUT 5
#endif

int SCUTEST_NUM_TESTS;
int SCUTEST_NUM_FIXTURES;
SCUTEST_FixtureInfo _SCUTEST_fixtures[SCUTEST_MAX_NUM_FIXTURES];

SCUTEST_TestInfo _SCUTEST_tests[SCUTEST_MAX_NUM_TESTS];

SCUTEST_FixtureInfo _SCUTEST_defaultSetUpTearDown = {.timeout = SCUTEST_DEFAULT_TIMEOUT};


struct SCUTEST_FailedTest {const SCUTEST_TestInfo* t; int index; int status;};
struct SCUTEST_State {
    struct SCUTEST_FailedTest failedTests[SCUTEST_MAX_NUM_TESTS];
    size_t SCUTEST_PASSED_COUNT;
    size_t SCUTEST_SKIPPED_COUNT;
    int SCUTEST_NUM_FAILED_TESTS;
};

static int SCUTEST_CHILD_PID;
static int SCUTEST_TTY_MASTER = -1;

int SCUTEST_getTTYMasterFd() {
    return SCUTEST_TTY_MASTER;
}

static inline const SCUTEST_FixtureInfo* SCUTEST_getFixtureForTest(const SCUTEST_TestInfo* test) {
    const SCUTEST_FixtureInfo * setUpTearDown = &_SCUTEST_defaultSetUpTearDown;
    for(int i = SCUTEST_NUM_FIXTURES - 1; i >= 0 ; i --)
        if(_SCUTEST_fixtures[i].lineNumber < test->lineNumber && strcmp(test->fileName, _SCUTEST_fixtures[i].fileName) == 0) {
            _SCUTEST_fixtures[i].flags |= setUpTearDown->flags;
            setUpTearDown = &_SCUTEST_fixtures[i];
            break;
        }
    return setUpTearDown;
}

static void SCUTEST_printResults(struct SCUTEST_State* state) {
    SCUTEST_PRINTF("....................\n");
    SCUTEST_PRINTF("Passed %ld/%ld tests; Skipped %d\n", state->SCUTEST_PASSED_COUNT, state->SCUTEST_PASSED_COUNT + state->SCUTEST_NUM_FAILED_TESTS, state->SCUTEST_SKIPPED_COUNT);
    for(int i = 0; i < state->SCUTEST_NUM_FAILED_TESTS; i++) {
        const SCUTEST_TestInfo* t = state->failedTests[i].t;
        int index = state->failedTests[i].index;
        char status = state->failedTests[i].status;
        SCUTEST_PRINTF("%s:%03d %s.%d (of %d) #%02d failed with status %hhd\n",
            t->fileName, t->lineNumber, t->name, index, t->iter ? t->iter : SCUTEST_getFixtureForTest(t)->iter, t->testNumber,
            status);
    }
}

#ifndef SCUTEST_NO_BUFFER
static int SCUTEST_drainBuffer(int fd, char** buffer, int*bufferSize, int*allocatedSize) {
    if(*buffer == NULL) {
        *buffer = malloc(SCUTEST_BUFFER_READ_SIZE);
        *allocatedSize = SCUTEST_BUFFER_READ_SIZE;
        *bufferSize = 0;
    } else if(*allocatedSize == *bufferSize) {
        *allocatedSize *= 2;
        *buffer = realloc(*buffer, *allocatedSize);
    }
    int result;
    result = read(fd, *buffer + *bufferSize, *allocatedSize - *bufferSize);
    if(result != -1)
        *bufferSize += result;
    return result;
}
#endif

static int SCUTEST_abort = 0;
static int SCUTEST_skip = 0;

static void SCUTEST_killChild() {
    SCUTEST_PRINTF("Aborting\n");
    if(kill(-SCUTEST_CHILD_PID, SIGKILL)) {
        SCUTEST_PERROR("Failed to kill child");
    }
}

static void SCUTEST_abort_testing(int signal) {
    SCUTEST_abort = signal;
    SCUTEST_PRINTF("Received external signal %d\n", signal);
    kill(getpid(), SIGALRM);
}

static void SCUTEST_skip_testing(int signal) {
    SCUTEST_skip = 1;
}

void SCUTEST_skipTest(const char* fmt, ...) {
    SCUTEST_skip = 1;
    SCUTEST_PRINTF("%c ", 0);
    if (fmt) {
        SCUTEST_VPRINTF(fmt);
    }
    SCUTEST_PRINTF("%c", 0);
}

static void SCUTEST_dump_backtrace(int signo, siginfo_t *info, void *ucontext) {
    switch(signo) {
        case SIGSEGV:
            SCUTEST_PRINTF("Segfault at address %p\n", info->si_addr);
            break;
    }
#ifdef __TCC_BACKTRACE__
    int tcc_backtrace(const char *fmt, ...);
    tcc_backtrace("Hit signal %d", signo);
#endif
    _exit(-signo);
}

static int SCUTEST_createSigHandler(int sig, void(*callback)(int)) {
    struct sigaction sa = {};
    sa.sa_handler = callback;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART | SA_NODEFER;
    return sigaction(sig, &sa, NULL);
}

static int SCUTEST_createSigAction(int sig, void(*callback)(int , siginfo_t *, void *)) {
    struct sigaction sa = {};
    sa.sa_sigaction = callback;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART | SA_NODEFER | SA_SIGINFO;
    return sigaction(sig, &sa, NULL);
}

static int getNumberOfFilesInDir(const char* dirname) {
    DIR* d = opendir(dirname);
    if (!d)
        return -1;
    struct dirent * dir;
    int count = 0;
    while ((dir = readdir(d)) != NULL) {
        if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
            continue;
        count++;
    }
    closedir(d);
    return count;
}

static int SCUTEST_getNumberOfOpenFds() {
    return getNumberOfFilesInDir("/proc/self/fd");
}

static int SCUTEST_runTest(struct SCUTEST_State* state, const SCUTEST_TestInfo* test, const SCUTEST_FixtureInfo * setUpTearDown, int i, int noFork, int noBuffer, int noSkip) {
    if(SCUTEST_abort)
        return 0;

    int fds[2];
    SCUTEST_PRINTF("%s:%03d %s.%d...", test->fileName, test->lineNumber, test->name, i);
    SCUTEST_FLUSH();

    int flags = (test->flags | setUpTearDown->flags) & ~test->rflags;

    if(noFork) {
        flags &= ~SCUTEST_CHECK_CHILD_LEAK;
    }
#ifndef SCUTEST_NO_STDLIB
    if (flags & SCUTEST_CREATE_TMP_DIR) {
        SCUTEST_createTempWorkingDir();
    }
#endif

#ifndef SCUTEST_NO_BUFFER
    if(!noBuffer) {
        pipe(fds);
    }
#endif

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    long startTimeMS = now.tv_sec * 1000 + now.tv_nsec / 1000000;
    if(noFork || !(SCUTEST_CHILD_PID = fork())) {
        if (!noFork) {
            setsid();
            signal(SIGINT, SIG_DFL);
        }
        if (flags & SCUTEST_CREATE_TTY) {
            if (SCUTEST_TTY_MASTER == -1) {
                SCUTEST_TTY_MASTER = posix_openpt(O_RDWR);
                if (SCUTEST_TTY_MASTER == -1 || grantpt(SCUTEST_TTY_MASTER) == -1 || unlockpt(SCUTEST_TTY_MASTER) == -1) {
                    SCUTEST_PERROR("Failed to create/grant/unlock tty");
                    SCUTEST_EXIT_FUNC(-1);
                }
            }

            struct winsize const win = {10, 10};
            ioctl(SCUTEST_TTY_MASTER, TIOCSWINSZ, &win);
            const char* name = ptsname(SCUTEST_TTY_MASTER);
            if (!name) {
                SCUTEST_PERROR("Failed to get name of tty");
                SCUTEST_EXIT_FUNC(-1);
            }
            int fd = open(name, O_RDWR);
            if (fd == -1 || dup2(fd, STDIN_FILENO) == -1){
                SCUTEST_PERROR("Failed to open/dup tty");
                SCUTEST_EXIT_FUNC(-1);
            }
            close(fd);
        }

#ifndef SCUTEST_NO_BUFFER
        if(!noBuffer) {
            dup2(fds[1], STDOUT_FILENO);
            dup2(fds[1], STDERR_FILENO);
            close(fds[1]);
            close(fds[0]);
        }
#endif

        int startingNumFds;
        if (flags & SCUTEST_CHECK_FD_LEAK) {
            startingNumFds = SCUTEST_getNumberOfOpenFds();
        }
        SCUTEST_createSigHandler(SIGALRM, SIG_DFL);
        pid_t pid = getpid();
        if(setUpTearDown->setUp)
            setUpTearDown->setUp(i);
        if (!SCUTEST_skip) {
            test->testFunc(i);
            if(setUpTearDown->tearDown)
                setUpTearDown->tearDown(i);
        }
        if (SCUTEST_skip) {
            if (!noFork && pid == getpid()) {
                kill(getppid(), SIGUSR1);
            }
        }
        if (pid != getpid()) {
            SCUTEST_EXIT_FUNC(0);
        }

        if (flags & SCUTEST_CHECK_FD_LEAK) {
            int numFds = SCUTEST_getNumberOfOpenFds();
            if (numFds > startingNumFds || ((flags & SCUTEST_CHECK_FD_LEAK_STRICT) && numFds != startingNumFds)) {
                SCUTEST_PRINTF("Expected number of fds %d vs %d\n", startingNumFds, numFds);
                SCUTEST_EXIT_FUNC(SCUTEST_FD_LEAK_EXIT_CODE);
            }
        }
        if (flags & SCUTEST_CHECK_CHILD_LEAK) {
            int ret = waitpid(-1, NULL, WNOHANG);
            if (ret != -1) {
                SCUTEST_PRINTF("Detected a %s process\n", ret ? "zombie process" : "child");
                SCUTEST_EXIT_FUNC(SCUTEST_CHILD_LEAK_EXIT_CODE);
            }
        }
        if(!noFork)
            SCUTEST_EXIT_FUNC(0);
    }
    char exitStatus = 0;
    if(!noFork) {
#ifndef SCUTEST_NO_BUFFER
        if(!noBuffer) {
            close(fds[1]);
        }
#endif
        alarm(test->timeout ? test->timeout : setUpTearDown->timeout ? setUpTearDown->timeout : _SCUTEST_defaultSetUpTearDown.timeout ? _SCUTEST_defaultSetUpTearDown.timeout : SCUTEST_DEFAULT_TIMEOUT);
    }
    int status = -1;
#ifndef SCUTEST_NO_BUFFER
    char* buffer = NULL;
    int bufferSize, allocatedSize;
    if(!noBuffer) {
        struct pollfd poll_fd = {fds[0], POLLIN};
        while(1) {
            int ret = poll(&poll_fd, 1, 100);
            if(!noFork && SCUTEST_CHILD_PID == waitpid(SCUTEST_CHILD_PID, &status, WNOHANG))
                break;
            if(ret > 0 && SCUTEST_drainBuffer(fds[0], &buffer, &bufferSize, &allocatedSize) == 0)
                break;
        }
    }
#endif
    if(!noFork) {
        if(status == -1 && waitpid(SCUTEST_CHILD_PID, &status, 0) == -1) {
            SCUTEST_PERROR("Failed to wait on child");
        }
        exitStatus = WIFEXITED(status) ? WEXITSTATUS(status) : WIFSIGNALED(status) ? WTERMSIG(status) : -1;
        alarm(0);
    }
    if (flags & SCUTEST_CHECK_CHILD_LEAK) {
        if(kill(-SCUTEST_CHILD_PID, 0) == 0) {
            SCUTEST_PRINTF("A child process still lives after the test finished; Exit status: %d\n", exitStatus);
            kill(-SCUTEST_CHILD_PID, SIGKILL);
            if (exitStatus == 0) {
                exitStatus = SCUTEST_CHILD_LEAK_EXIT_CODE;
            }
        }
    }
    clock_gettime(CLOCK_MONOTONIC, &now);
    long delta = now.tv_sec * 1000 + now.tv_nsec / 1000000 - startTimeMS ;
    int passed = SCUTEST_skip || SCUTEST_abort ? 0 : exitStatus == test->exitCode;
    if (noSkip && SCUTEST_skip) {
        SCUTEST_skip = 0;
    }
    const char * extra_status = "";
    int extra_status_len = 0;
    if (SCUTEST_skip) {
        state->SCUTEST_SKIPPED_COUNT++;
#ifndef SCUTEST_NO_BUFFER
        for (int i = 0; buffer && i < bufferSize - 1; i++) {
            if (!buffer[i]) {
                extra_status = buffer + i + 1;
                extra_status_len = extra_status - buffer + bufferSize;
            }
        }
#endif
    }
    SCUTEST_PRINTF("%s (%dms)%.*s\n", SCUTEST_skip ? "skipped" : passed ? "passed" : "failed", delta, extra_status_len, extra_status);
    if (!SCUTEST_skip) {
        if(!passed) {
            state->failedTests[state->SCUTEST_NUM_FAILED_TESTS++] = (struct SCUTEST_FailedTest) {test, i, exitStatus};
        }
        state->SCUTEST_PASSED_COUNT += passed;
    }
#ifndef SCUTEST_NO_BUFFER
    if(!noBuffer) {
        close(fds[0]);
        if(!passed && !SCUTEST_skip && bufferSize)
            write(STDOUT_FILENO, buffer, bufferSize);
        free(buffer);
    }
#endif
#ifndef SCUTEST_NO_STDLIB
    if (flags & SCUTEST_CREATE_TMP_DIR) {
        destroyTempWorkingDir();
    }
#endif
    if (SCUTEST_skip) {
        SCUTEST_skip = 0;
        return 1;
    }
    return passed;
}

static int runUnitTestsWithCustomStateAndSettings(struct SCUTEST_State* state, const ScutestSettings settings) {
    for(int i = 0; i < SCUTEST_NUM_TESTS && !SCUTEST_abort; i++) {
        SCUTEST_TestInfo* t = _SCUTEST_tests + i;
        if((!settings.file || strcmp(settings.file, t->fileName) == 0) && (!settings.func || strcmp(settings.func, t->name) == 0)) {
            const SCUTEST_FixtureInfo * setUpTearDown = SCUTEST_getFixtureForTest(t);
            for(int r = 0; (r < settings.repeat || !r) && !(settings.strict && state->SCUTEST_NUM_FAILED_TESTS); r++) {
                if(settings.index >= 0)
                    SCUTEST_runTest(state, t, setUpTearDown, settings.index, settings.noFork, settings.noBuffer, settings.noSkip);
                else
                    for(int i = 0; i < (t->iter ? t->iter : setUpTearDown->iter ? setUpTearDown->iter : 1); i++)
                        if(!SCUTEST_runTest(state, t, setUpTearDown, i, settings.noFork, settings.noBuffer, settings.noSkip) && settings.strict >= 2)
                            break;
            }
        }
    }
    SCUTEST_printResults(state);
    return state->SCUTEST_NUM_FAILED_TESTS ? 1 : 0;
}

int runUnitTestsWithSettings(const ScutestSettings settings) {
    struct SCUTEST_State state = {};
    return runUnitTestsWithCustomStateAndSettings(&state, settings);
}

static int runUnitTestsWithState(struct SCUTEST_State * state) {
    ScutestSettings settings = {
        .file = SCUTEST_GETENV_FUNC("TEST_FILE"),
        .func = SCUTEST_GETENV_FUNC("TEST_FUNC"),
        .noFork = !!SCUTEST_GETENV_FUNC("NO_FORK"),
        .noBuffer = !!SCUTEST_GETENV_FUNC("NO_BUFFER"),
        .noSkip = !!SCUTEST_GETENV_FUNC("NO_SKIP"),
        .strict = SCUTEST_ATOI(SCUTEST_GETENV_FUNC_OR_DEFAULT("STRICT", "0")),
        .repeat = SCUTEST_ATOI(SCUTEST_GETENV_FUNC_OR_DEFAULT("REPEAT", "0")),
    };
    char* index = settings.func ? strchr(settings.func, '.') : NULL;
    if(index) {
        *index = 0;
        index++;
    }
    settings.index = index ? SCUTEST_ATOI(index) : -1;
    return runUnitTestsWithCustomStateAndSettings(state, settings);
}

int runUnitTests() {
    SCUTEST_createSigHandler(SIGALRM, SCUTEST_killChild);
    SCUTEST_createSigHandler(SIGINT, SCUTEST_abort_testing);
    SCUTEST_createSigHandler(SIGUSR1, SCUTEST_skip_testing);
    SCUTEST_createSigAction(SIGSEGV, SCUTEST_dump_backtrace);
    SCUTEST_createSigAction(SIGABRT, SCUTEST_dump_backtrace);

    struct SCUTEST_State state = {};
    return runUnitTestsWithState(&state);
}
#endif
