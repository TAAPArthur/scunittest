#define SCUTEST_DEFINE_MAIN
#define SCUTEST_IMPLEMENTATION
#define SCUTEST_DEFAULT_TIMEOUT 2
#include "scutest.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static setupCounter = 0;
void defaultSetup() {
    setupCounter++;
}

SCUTEST(test_default_setup_defined_before_default) {
    assert(setupCounter);
}

SCUTEST_SET_DEFAULT_FIXTURE(defaultSetup, NULL, 0);

SCUTEST(test_default_setup) {
    assert(setupCounter);
}

SCUTEST_SET_FIXTURE_NO_ARGS(NULL, NULL);

SCUTEST(test_default_setup_override) {
    assert(!setupCounter);
}

SCUTEST(test_assert) {
    printf("THIS_MESSAGE_SHOULD_NOT_BE_SEEN");
    assert(1);
}

SCUTEST(test_assert_index) {
    assert(_i == 0);
}

/**
 * Each test runs as its own process so they each start from the same state
 */
static int value;
SCUTEST_ITER(test_fork, 2) {
    assert(value++ == 0);
}

SCUTEST_ERR(test_err, 2) {
    exit(2);
}

SCUTEST(test_large_output) {
    for(int i = 0; i < 100000; i++)
        printf("yes");
}

SCUTEST_SET_FIXTURE_NO_ARGS(NULL, NULL);
SCUTEST_NO_ARGS(test_assert_no_args) {
    assert(1);
}

static void setUp() {
    value = 1;
}
static void tearDown() {
    if(value != 1)
        exit(6);
}
SCUTEST_SET_FIXTURE(setUp, tearDown);

SCUTEST(test_setup_fork) {
    assert(value == 1);
}
SCUTEST_ERR(test_teardown, 6) {
    assert(value++ == 1);
}
static int savedIndex;
static void setUpI(int i) {
    savedIndex = i;
}
static void tearDownI(int i) {
    assert(savedIndex == i);
}

SCUTEST_SET_FIXTURE(setUpI, tearDownI);
SCUTEST(test_setup_index, .iter = 2) {
    assert(_i == savedIndex);
}

SCUTEST_SET_FIXTURE(setUpI, tearDownI, .iter=2)

SCUTEST(test_setup_index_from_fixture) {
    assert(_i == savedIndex);
}

SCUTEST(test_setup_index_from_fixture_override, .iter=1) {
    assert(0 == savedIndex);
}

SCUTEST_SET_FIXTURE(NULL, NULL);

SCUTEST(test_noop_with_flags, .flags=SCUTEST_CREATE_TMP_DIR|SCUTEST_CHECK_FD_LEAK) {}

SCUTEST(test_fd_leak, .flags=SCUTEST_CREATE_TMP_DIR|SCUTEST_CHECK_FD_LEAK, .exitCode = SCUTEST_FD_LEAK_EXIT_CODE) {
    char name[SCUTEST_TEMP_FILE_NAME_MAX_LEN];
    int fd = open(SCUTEST_TEMP_WORKING_DIR, O_RDONLY | O_DIRECTORY);
    assert(fd != -1);
    // leak fd
}

// If the fds change, consider it a leak
SCUTEST(test_fd_leak_strict, .flags=SCUTEST_CREATE_TTY | SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_FD_LEAK_STRICT, .exitCode = SCUTEST_FD_LEAK_EXIT_CODE) {
    close(STDIN_FILENO);
}

// Normally it is fine if we end with fewer fds
SCUTEST(test_fd_leak_nostrict, .flags=SCUTEST_CREATE_TTY | SCUTEST_CHECK_FD_LEAK ) {
    close(STDIN_FILENO);
}

SCUTEST(test_create_temp_file, .flags=SCUTEST_CREATE_TMP_DIR | SCUTEST_CHECK_FD_LEAK) {
    char name[SCUTEST_TEMP_FILE_NAME_MAX_LEN];
    int fd = open(SCUTEST_TEMP_WORKING_DIR, O_RDONLY | O_DIRECTORY);
    assert(fd != -1);
    close(fd);
    int ret = createTempFileForTest(name);
    assert(ret != -1);
    close(ret);
}

SCUTEST(test_tty, .flags=SCUTEST_CREATE_TTY, .iter = 2) {
    assert(isatty(_i ? STDIN_FILENO : SCUTEST_getTTYMasterFd()));
}

SCUTEST(test_child_leak, .flags=SCUTEST_CHECK_CHILD_LEAK, .iter = 2, .exitCode = SCUTEST_CHILD_LEAK_EXIT_CODE) {
    if(!fork()) {
        if(_i)
            exit(0);
    }
}

SCUTEST_SET_FIXTURE(NULL, NULL, .flags = SCUTEST_CHECK_CHILD_LEAK | SCUTEST_CREATE_TMP_DIR|SCUTEST_CHECK_FD_LEAK);
SCUTEST(test_fd_leak_allow, .rflags = SCUTEST_CHECK_FD_LEAK) {
    assert(open(SCUTEST_TEMP_WORKING_DIR, O_RDONLY | O_DIRECTORY) != -1);
}

SCUTEST(test_child_leak_allow, .rflags = SCUTEST_CHECK_CHILD_LEAK ) {
    fork();
}


/* Clear fixuture */
SCUTEST_SET_FIXTURE(NULL, NULL);
/**
 * Sleep of mil milliseconds
 * @param ms number of milliseconds to sleep
 */
static inline void msleep(int ms) {
    struct timespec ts;
    ts.tv_sec = ms / 1000;
    ts.tv_nsec = (ms % 1000) * 1000000;
    while (nanosleep(&ts, &ts));
}

/*
 * When tests timeout they exit with status 9
 */
SCUTEST(test_timeout_default_test, .exitCode = 9) {
    msleep(40000);
    assert(0);
}
SCUTEST(test_timeout_single_test, .exitCode = 9, .timeout = 1) {
    msleep(4000);
    assert(0);
}

static void skipFixture(int i) {
    if (i == 0) {
        SCUTEST_skipTest("Intentionally skipping test %d", i);
    }
}

SCUTEST_SET_FIXTURE(skipFixture, NULL, .iter = 2);
SCUTEST(test_skip) {
    assert(_i);
    setenv("TEST_FUNC", "test_skip", 1);
    static pid;
    if (pid) {
        assert(pid != getpid());
        return;
    }

    struct SCUTEST_State state = {};
    pid = getpid();
    assert(!runUnitTestsWithState(&state));
    assert(state.SCUTEST_PASSED_COUNT == 1);
    assert(state.SCUTEST_SKIPPED_COUNT == 1);
}

SCUTEST_SET_FIXTURE(NULL, NULL, .timeout = 1);
SCUTEST_ITER_ERR(test_timeout, 2, 9) {
    msleep(4000);
    assert(0);
}

SCUTEST(test_kill_abandoned_child_success, .iter=2, .exitCode = SCUTEST_CHILD_LEAK_EXIT_CODE, .flags=SCUTEST_CHECK_CHILD_LEAK) {
    if(!fork()) {
        if(_i)
            fork();
        while(1)
            msleep(40000);
    }
}

SCUTEST(test_kill_abandoned_child_failure, .timeout = 100, .exitCode = -6, .flags=SCUTEST_CHECK_CHILD_LEAK) {
    if(!fork()) {
        msleep(40000);
    }
    assert(0);
}


SCUTEST(test_exit_status) {
    static nested = 0;
    if (nested) {
        exit(1);
    }
    nested = 1;
    assert(1 == runUnitTestsWithSettings((ScutestSettings){.file=__FILE__, .func="test_exit_status"}));
}

SCUTEST(test_repeat) {
    static nested = 0;
    assert(_i == 0);
    if (nested) {
        exit(0);
    }
    nested = 1;
    int repeat = 10;

    struct SCUTEST_State state = {};
    runUnitTestsWithCustomStateAndSettings(&state,(ScutestSettings){.file=__FILE__, .func="test_repeat", .repeat = repeat});
    assert(state.SCUTEST_PASSED_COUNT == repeat);
}

SCUTEST(test_specific_index) {
    if(_i == 100)
        return;
    setenv("TEST_FUNC", "test_specific_index.100", 1);
    assert(!runUnitTests());
}

SCUTEST(test_sigint) {
    if(_i == 100) {
        kill(getppid(), SIGINT);
        msleep(4000);
        return;
    }
    setenv("TEST_FUNC", "test_sigint.100", 1);
    assert(runUnitTests());
}

SCUTEST_SET_FIXTURE(NULL, NULL, .flags = (SCUTEST_CREATE_TMP_DIR | SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_CHILD_LEAK));
SCUTEST(test_no_fork) {
    setenv("NO_FORK", "1", 1);
    setenv("NO_BUFFER", "1", 1);
    setenv("TEST_FILE", __FILE__, 1);
    setenv("TEST_FUNC", "test_no_fork", 1);
    static pid;
    if (pid) {
        assert(pid == getpid());
        return;
    }
    pid = getpid();
    assert(!runUnitTests());
}
